#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::ffi::CString;

#[derive(Debug, Clone)]
pub struct EmacsString(CString);

impl EmacsString {
    pub fn new(content: &str) -> Self {
        Self(CString::new(content).unwrap())
    }
    pub fn as_ptr(&self) -> *const i8 {
        self.0.as_ptr()
    }
}

impl From<&str> for EmacsString {
    fn from(src: &str) -> Self {
        Self::new(src)
    }
}
impl Default for EmacsString {
    fn default() -> Self {
        Self::new("")
    }
}

/// Get a mutable reference to the environment attached to the runtime
pub fn extract_env_from_runtime_mut(ert: &mut emacs_runtime) -> &mut emacs_env {
    // Safety
    // Trusting Emacs API on this
    unsafe {
        ert.get_environment.expect("No get_environment function")(ert)
            .as_mut()
            .unwrap()
    }
}

/// Return a symbol in the given Emacs environment.
///
/// # Panic
/// This function panic if the name has invalid characters for an emacs symbol
pub fn emacs_symbol(env: &mut emacs_env, name: EmacsString) -> emacs_value {
    // Safety : we trust the emacs API
    unsafe { env.intern.unwrap()(env, name.as_ptr()) }
}

/// Return a integer value for the given Emacs environment.
pub fn emacs_integer(env: &mut emacs_env, value: i64) -> emacs_value {
    // Safety : we trust the emacs API
    unsafe { env.make_integer.unwrap()(env, value) }
}

/// Create a symbol emacs_value in the given env
///
/// # Panics
/// Panics if the environment cannot create the symbol.
pub fn make_symbol(env: &mut emacs_env, name: EmacsString) -> emacs_value {
    // Safety : we trust the emacs API
    unsafe { env.intern.unwrap()(env, name.as_ptr()) }
}

/// Call a function in the given env
/// Funcall wrapper
///
/// # Panics
/// Panics if the args len doesn't fit an isize
pub fn emacs_funcall(
    env: &mut emacs_env,
    f_name: EmacsString,
    args: &mut [emacs_value],
) -> emacs_value {
    // Safety : we trust the emacs API
    unsafe {
        env.funcall.unwrap()(
            env,
            emacs_symbol(env, f_name),
            args.len() as isize,
            args.as_mut_ptr(),
        )
    }
}

/// Create a function in the given env
///
/// # Panics
/// Panics if the environment cannot create the symbol.
pub fn make_function(
    env: &mut emacs_env,
    min_arity: isize,
    max_arity: isize,
    proc: emacs_function,
    docstring: EmacsString,
    extra_data: *mut libc::c_void,
) -> emacs_value {
    // Safety : we trust the emacs API
    unsafe {
        env.make_function.unwrap()(
            env,
            min_arity,
            max_arity,
            proc,
            // TODO: understand why using an Option there breaks the ownership.
            // When docstring is an option and we use
            // doctring.map_or(std::ptr::null_mut(), EmacsString::as_ptr), I get weird errors on
            // loading the module about utf-8-string-p. To check.
            docstring.as_ptr(),
            extra_data,
        )
    }
}

/// Bind a function to name in current env
pub fn bind_function(
    env: &mut emacs_env,
    name: EmacsString,
    min_arity: isize,
    max_arity: isize,
    proc: emacs_function,
    docstring: Option<EmacsString>,
    extra_data: *mut libc::c_void,
) -> emacs_value {
    let func_value = make_function(
        env,
        min_arity,
        max_arity,
        proc,
        docstring.unwrap_or_default(),
        extra_data,
    );
    let func_symbol = make_symbol(env, name);
    let mut args: [emacs_value; 2] = [func_symbol, func_value];
    emacs_funcall(env, EmacsString::new("defalias"), &mut args)
}
