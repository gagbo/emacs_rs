# Rust bindings for Emacs' dynamic modules

This is a learning experiment in FFI world.

## Crude roadmap

- generate raw bindings with bindgen
- export the correct `plugin_is_GPL_compatible` plugin
- load succesfully a `.so` in Emacs (calling the module initialization function)
- write and export a function that takes no parameter and return a single value
- write and export a function that takes one parameter, check its type and return a value
- Clean up the code to try to reduce unsafe exposure
- write and export a function that takes variadic parameters and return a value
- abstract the necessary boilerplate code in reusable `unsafe` wrappers


## Current state
Once you build the project and copy the `libsimple.so` file in your Emacs
`load-path`, you can try

``` emacs-lisp
(require 'libsimple)
;; Returns 18
(emacs-rs--naive)
;; Errors out because no arg
(emacs-rs--oddp)
;; Errors out because not a number
(emacs-rs--oddp "nan")
;; is t
(emacs-rs--oddp 1)
;; is nil
(emacs-rs--oddp 2)
```

## Wrapping the binary

Calling a bare `(require 'libemacs_rs)` will make Emacs unhappy as it expects a
require to `(provide)` the feature to avoid subsequent loading. I think the best
solution is to wrap this call inside a lisp library :

``` emacs-lisp
;; your library

(cond ((memq system-name '(ms-dos windows-nt cygwin))
       (load 'libemacs_rs.dll'))
      ((memq system-name '(darwin))
       (load 'libemacs_rs.dylib'))
      (t (load 'libemacs_rs.so')))
(provide 'emacs_rs)
```
