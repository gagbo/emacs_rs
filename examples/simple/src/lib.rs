// Copyright (C) 2020 Gerry Agbobada
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Example crate that provide very simple functions
use emacs_rs::bindings::{
    self, bind_function, emacs_integer, emacs_symbol, extract_env_from_runtime_mut,
};
use libc::c_int;
use std::{mem::size_of, slice};

#[no_mangle]
static plugin_is_GPL_compatible: c_int = 1;

#[no_mangle]
pub extern "C" fn emacs_module_init(ert: *mut bindings::emacs_runtime) -> c_int {
    // Safety
    // This is the single entrypoint of Emacs while loading the module, so taking ownership here
    // is safe as long as Emacs doesn't give the pointer to another process/thread.
    let ert = unsafe { ert.as_mut().unwrap() };

    let env = extract_env_from_runtime_mut(ert);
    if env.size as usize >= size_of::<bindings::emacs_env_28>() {
        // Emacs 28 or later

        // // Bind naive_func
        let _naive_func = bind_function(
            env,
            "emacs-rs--naive".into(),
            0,
            0,
            Some(raw_naive_function),
            Some("Test function that should return 18.".into()),
            std::ptr::null_mut(),
        );

        // // Bind is_odd
        let _is_odd = bind_function(
            env,
            "emacs-rs--oddp".into(),
            1,
            1,
            Some(raw_is_odd_function),
            Some("Test function that return if the given value is odd.".into()),
            std::ptr::null_mut(),
        );
        0
    } else {
        2 // Unknown or unsupported version.
    }
}

fn naive_function() -> usize {
    19
}

/// Raw wrapper to naive_function
///
/// # Safety
///
/// ## Taking ownership of env
/// FIXME To prove
#[no_mangle]
pub unsafe extern "C" fn raw_naive_function(
    env: *mut bindings::emacs_env,
    _nargs: isize,
    _args: *mut bindings::emacs_value,
    _data: *mut libc::c_void,
) -> bindings::emacs_value {
    let env = env.as_mut().unwrap();
    let retval = naive_function() as isize;
    emacs_integer(env, retval as i64)
}

fn is_odd(value: isize) -> bool {
    value % 2 == 1
}

/// Raw wrapper to is_odd
///
/// # Safety
///
/// ## Taking ownership of env
/// FIXME To prove
///
/// ## Building a slice from raw parts
/// Arity for the array is ensured by the make_function call that wraps this funtion
/// Extract_integer will raise an Emacs error if the argument has the wrong type
#[no_mangle]
pub unsafe extern "C" fn raw_is_odd_function(
    env: *mut bindings::emacs_env,
    nargs: isize,
    args: *mut bindings::emacs_value,
    _data: *mut libc::c_void,
) -> bindings::emacs_value {
    let env = env.as_mut().unwrap();

    let first_arg = slice::from_raw_parts(args, nargs as usize)[0];
    let value = env.extract_integer.unwrap()(env, first_arg);

    let retval = if is_odd(value as isize) { "t" } else { "nil" };
    emacs_symbol(env, retval.into())
}
